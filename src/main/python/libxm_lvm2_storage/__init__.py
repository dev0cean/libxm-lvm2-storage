import os
import dateutil.parser
from libxm import ssh
from libxm.exceptions import XenmasterError
from libxm.model.storage import XenmasterStorageDriver
from libxm.service.storage_service import DiskImageExistsException, DiskImageInUseException, \
    get_disk_image_with_name
from ocsutil import console


def _get_control_host(repository):
    """
    :type repository: libxm.model.StorageRepository
    :return:
    """
    for cluster in repository.clusters:
        return cluster.master
    raise XenmasterError("'%s' is not associated with any cluster" % repository.name)


def _host_mounts(host, disk_image):
    mounts = 0
    for guest in host.guests:
        for device in guest.block_devices.itervalues():
            if device.disk_image == disk_image and device.connected:
                mounts += 1
    return mounts


class LVM2StorageDriver(XenmasterStorageDriver):
    impl = "lvm2"
    name = "LVM2 Volume Group Storage Driver"
    description = """
        Enables the use of LVM2 volume groups as physical containers for
        disk images represented by logical volumes.
        Each host must use the same volume group name such that on any
        host the absolute path to a given disk image's logical volume
        is the same.
    """
    scheme = "phy"

    @staticmethod
    def repository_exists(repository):
        def vg_exists(host):
            command = "vgscan | grep -q \"'%s'\"" % repository.name
            return ssh.execute("root@" + host.fqdn, command).succeeded
        return any(ssh.call(repository.hosts, vg_exists))

    def get_default_repository_path(self, name, driver_parameters):
        return "/dev/" + name

    def create_repository(self, repository, cluster, driver_parameters):
        if self.repository_exists(repository):
            raise XenmasterError("The '{repository.name}' storage repository already exists".format(repository))

        control_host = _get_control_host(repository)
        physdev = driver_parameters.get("physdev")
        pv_exists = ssh.execute("root@" + control_host.fqdn, "pvs %s" % physdev)
        if pv_exists.failed:
            console.debug("Creating LVM PV %s on %s." % (physdev, control_host.name))
            pvcreate = ssh.execute("root@" + control_host.fqdn, "pvcreate %s" % physdev)
            if pvcreate.failed:
                raise XenmasterError("Failed to create LVM PV %s on %s." % (physdev, control_host.name),
                                     pvcreate.stderr)
        vgcreate = ssh.execute("root@" + control_host.fqdn, "vgcreate %s %s" % (repository.name, physdev))
        console.debug("Creating LVM volume group %s on %s." % (repository.name, control_host.name))
        if vgcreate.failed:
            raise XenmasterError("Failed to create volume group %s on %s." % (repository.name, control_host.name),
                                 vgcreate.stderr)
        self.sync_repository(repository)

    def attach_repository(self, repository, cluster):
        """
        :type repository: libxm.model.StorageRepository
        :type cluster: libxm.model.Cluster
        """
        # todo: call crm to enable the VG
        self.sync_repository(repository)

    def detach_repository(self, repository, cluster):
        """
        :type repository: libxm.model.StorageRepository
        :type cluster: libxm.model.Cluster
        """
        # todo: call crm to disable the VG
        pass

    @staticmethod
    def _sync_volume_group(repository):
        control_host = _get_control_host(repository)
        vgs = ssh.execute("root@" + control_host.fqdn,
                          "vgs --units m --nosuffix --noheadings --separator '|' %s -o "
                          "vg_uuid,vg_name,vg_size,vg_free,lv_count" % repository.name)
        if vgs.failed:
            raise XenmasterError("Failed to sync volume group '%s' on '%s'." % (repository.name, control_host.name),
                                 vgs.stderr)
        uuid, name, size, free, lv_count = vgs.split('|')
        repository.allocation = long(float(size))
        repository.utilisation = repository.allocation - long(float(free))
        repository.device_id = uuid
        repository.tags["uuid"] = uuid
        repository.tags["images"] = lv_count
        repository.name = name

    def sync_repository(self, repository):
        """
        :type repository: libxm.model.StorageRepository
        """
        def _sync_connection(host):
            vgdisplay = ssh.execute("root@" + host.fqdn, "vgs --noheadings %s" % repository.path)
            if vgdisplay.failed:
                # remove physical device connection if one exists
                if host in repository.hosts:
                    repository.hosts.remove(host)
            else:
                if not host in repository.hosts:
                    repository.hosts.add(host)

        try:
            self._sync_volume_group(repository)
            for cluster in repository.clusters:
                ssh.call(cluster.online_hosts(), _sync_connection)
        except Exception, e:
            raise XenmasterError("Unable to sync volume group '%s'" % repository.name, e)

    @staticmethod
    def image_exists(image):
        def device_exists(host):
            command = "lvscan | grep -q \"'%s/%s'\"" % (image.repository.path, image.location)
            return ssh.execute("root@" + host.fqdn, command).succeeded
        return any(ssh.call(image.repository.hosts, device_exists))

    def create_disk_image(self, image):
        """
        :type image: libxm.model.VirtualDiskImage
        """
        # shared LVM VGs use Clustered LVM so the image is only created on one host
        # and then activated on each host
        # perform a simple check that the image doesn't exist first to distinguish
        # between this failure and other possible failures
        # it is possible, if highly unlikely, that the physdev path will exist on a
        # subset of repository hosts
        if self.image_exists(image):
            raise DiskImageExistsException("The '{image.repository.name}' storage repository already "
                                           "contains a disk image named '{image.location}'".format(image=image))

        lvcreate = ssh.execute("root@" + (_get_control_host(image.repository)).fqdn,
                               "lvcreate -L {image.virtual_size}m -n {image.location} {image.repository.name} && "
                               "lvchange -anl {image.repository.name}/{image.location}"
                               .format(image=image))
        if lvcreate.failed:
            raise XenmasterError("The disk image '{image.name}' could not be created in the '{image.repository.name}' "
                                 "repository.".format(image=image), lvcreate.stderr)
        self.sync_disk_image(image)
        self._sync_volume_group(image.repository)

    def clone_disk_image(self, src, dst, show_progress=False):
        if self.image_exists(dst):
            raise DiskImageExistsException("The '{image.repository.name}' storage repository already "
                                           "contains a disk image named '{image.location}'".format(image=dst))
        self.sync_disk_image(src)
        if not src.present:
            raise XenmasterError("The source disk image '%s' for a clone could not be found" % src.qualified_name)
        if any(device.connected for device in src.block_devices) and not src.read_only:
            raise XenmasterError("Unable to clone '%s'" % src.qualified_name,
                                 "You are currently able to clone a mounted disk image only if it is write-protected.")
        control_host = _get_control_host(dst.repository)
        self.create_disk_image(dst)
        self.attach_disk_image(dst, control_host)
        mount_required = _host_mounts(control_host, src) == 0
        try:
            if mount_required:
                self.attach_disk_image(src, control_host)
            if show_progress:
                dd_cmd = "dd if={src.repository.path}/{src.location} bs=4M iflag=fullblock 2>/dev/null" \
                         " | pv -f -s {src.virtual_size}m" \
                         " | dd iflag=fullblock bs=4M of={dst.repository.path}/{dst.location} 2>/dev/null"
            else:
                dd_cmd = "dd if={src.repository.path}/{src.location} of={dst.repository.path}/{dst.location} bs=4M"
            copy = ssh.execute("root@" + control_host.fqdn, dd_cmd.format(src=src, dst=dst))
            if copy.failed:
                try:
                    self.destroy_disk_image(dst)
                except:
                    pass
                raise Exception(copy.stderr)
        except Exception, cause:
            raise XenmasterError("The disk image '{src.name}' could not be cloned as '{dst.name}' in the "
                                 "'{src.repository.name}' repository.".format(src=src, dst=dst), cause)
        finally:
            if mount_required:
                self.detach_disk_image(src, control_host)
        self.detach_disk_image(dst, control_host)
        self.sync_disk_image(dst)
        self._sync_volume_group(dst.repository)

    def destroy_disk_image(self, image):
        if not self.image_exists(image):
            image.present = False
            return
        control_host = _get_control_host(image.repository)
        if len(image.block_devices) > 0:
            raise DiskImageInUseException("Refusing to destroy disk image '%s' as it has mapped block devices",
                                          guests=[device.guest for device in image.block_devices])
        self.detach_disk_image(image)
        lv_path = "%s/%s" % (image.repository.path, image.location)
        lvremove = ssh.execute("root@" + control_host.fqdn, "lvremove %s" % lv_path)
        if lvremove.failed:
            raise Exception(
                "The disk image '%s' could not be removed from the '%s' repository." %
                (image.name, image.repository.name), lvremove.stderr)
        self._sync_volume_group(image.repository)
        image.repository = None

    def sync_disk_image(self, image):
        control_host = _get_control_host(image.repository)
        lvs = ssh.execute("root@" + control_host.fqdn,
                          "lvs --noheadings --units m --nosuffix --separator '|' %s/%s -o "
                          "lv_uuid,lv_name,lv_path,lv_size,origin,data_percent,lv_time" %
                          (image.repository.name, image.location))
        if lvs.failed:
            image.present = False
            return
        image.present = True
        uuid, name, path, size, parent, utilisation, create_time = lvs.split('|')
        image.virtual_size = int(float(size))
        image.physical_size = image.virtual_size
        image.tags["lv_uuid"] = uuid
        image.name = name
        image.location = os.path.relpath(path, image.repository.path)
        if parent:
            image.parent = get_disk_image_with_name(parent, image.repository)
        if create_time:
            image.created = dateutil.parser.parse(create_time).utctimetuple()

    def attach_disk_image(self, image, host):
        self.sync_disk_image(image)
        activation = ssh.execute(
            "root@" + host.fqdn, "lvchange -aly %s/%s" % (image.repository.path, image.location))
        if activation.failed:
            raise XenmasterError("Failed to activate %s/%s on %s" % (image.repository.path, image.location, host.name),
                                 activation.stderr)

    def detach_disk_image(self, image, host=None):
        if not self.image_exists(image):
            image.present = False
            return
        target_host = host or _get_control_host(image.repository)
        deactivation = ssh.execute(
            "root@" + target_host.fqdn,
            "lvchange -a%sn %s/%s" % ('l' if host else '', image.repository.path, image.location)
        )
        if deactivation.failed:
            raise DiskImageInUseException(
                "%s/%s could not be deactivated on %s" % (image.repository.path, image.location, target_host.name),
                "Check that it or one of its partitions (if it is partitioned) are not mounted\n%s" %
                deactivation.stderr)

    def resize_disk_image(self, image, new_size):
        if not self.image_exists(image):
            raise XenmasterError("The disk image '{image.name}' in the '{image.repository.name}' repository"
                                 " could not be resized.".format(image=image), "Disk image not found.")

        lvcreate = ssh.execute("root@" + (_get_control_host(image.repository)).fqdn,
                               "lvcreate -L {image.virtual_size}m -n {image.location} {image.repository.name} && "
                               "lvchange -anl {image.repository.name}/{image.location}"
                               .format(image=image))
        if lvcreate.failed:
            raise XenmasterError("The disk image '{image.name}' could not be created in the '{image.repository.name}' "
                                 "repository.".format(image=image), lvcreate.stderr)
        self.sync_disk_image(image)
        self._sync_volume_group(image.repository)

        control_host = _get_control_host(image.repository)
        lvs = ssh.execute("root@" + control_host.fqdn,
                          "lvs --noheadings --units m --nosuffix --separator '|' %s/%s -o "
                          "lv_uuid,lv_name,lv_path,lv_size,origin,data_percent,lv_time" %
                          (image.repository.name, image.location))
        if lvs.failed:
            image.present = False
            return
        image.present = True
        uuid, name, path, size, parent, utilisation, create_time = lvs.split('|')
        image.virtual_size = int(float(size))
        image.physical_size = image.virtual_size
        image.tags["lv_uuid"] = uuid
        image.name = name
        image.location = os.path.relpath(path, image.repository.path)
        if parent:
            image.parent = get_disk_image_with_name(parent, image.repository)
        if create_time:
            image.created = dateutil.parser.parse(create_time).utctimetuple()
