#!/usr/bin/env python
import os
from setuptools import setup, find_packages
import sys

if sys.platform == 'darwin' and [int(x) for x in os.uname()[2].split('.')] >= [11, 0, 0]:
    # clang is serious now
    extra_compile_args = ['-Werror', '-Wunused-command-line-argument-hard-error-in-future', '-Qunused-arguments']
else:
    extra_compile_args = ['-Werror']


setup(
    name="libxm_lvm2_storage",
    version="1.2.0",
    description="",
    package_dir={'': "src/main/python"},
    packages=find_packages("src/main/python"),
    entry_points={
        "libxm.plugins": [
            "storage = libxm_lvm2_storage:LVM2StorageDriver"
        ]
    },
    dependency_links=[
        "git+ssh://git@bitbucket.org/dev0cean/libxm.git@1.2.0#egg=libxm-1.2.0",
        "https://bitbucket.org/dwhitla/python-ocsutil/get/1.0.4.zip#egg=ocsutil-1.0.4",
    ],
    install_requires=[
        "ocsutil>=1.0.4",
        "python-dateutil",
        "libxm>=1.2.0",
    ],
    url='',
    license='',
    author="dwhitla",
    author_email="dave.whitla@ocean.net.au",
    classifiers=[
        "Programming Language :: Python :: 2",
        "Development Status :: 3 - Alpha",
        "Topic :: Software Development :: Libraries :: Python Modules",
    ],
)
